<?php
// $Id$


//@todo add a different update and insert
//  update should load then modify
/**
 * Implements hook_field_storage_pre_insert().
 */
function _personal_taxonomy_field_storage_pre_insert($entity_type, $entity, &$skip_fields) {
  _personal_taxonomy_field_storage_pre_update($entity_type, $entity, $skip_fields);
}

/**
 * Implements hook_field_storage_pre_update().
 */
function _personal_taxonomy_field_storage_pre_update($entity_type, $entity, &$skip_fields) {
  $ptfields = variable_get('personal_taxonomy', array());
  if(!empty($ptfields)){
    if(!isset($entity->type))
      $type = $entity_type;
    else
      $type = $entity->type;
    foreach ($ptfields as $etype => $bundles){
      //check if the entity has a personal_taxonomy field in it.
      if ($entity_type == $etype){
        foreach ($bundles as $bundle => $fields){
          if (!empty($fields) && $bundle == $type){
            foreach ($fields as $key => $field) {
              if(isset($entity->{$key}['und'][0])) {
                foreach ($entity->{$key}['und'] as $tkey => $term) {
                  $tids[] = $term['tid'];
                }
                $ptentities = entity_load('personal_taxonomy', FALSE, array('uid'=>$entity->uid));
                if (empty($ptentities)){
                  _personal_taxonomy_create_entity($entity->uid, $tids);
                }
                else {
                  //@todo is there a more efficient way of doing this next part?
                  foreach ($ptentities as $k => $ptentity) {
                    $pttids[] = $ptentity->tid; 
                  }
                  $newtids = array_diff($tids, $pttids);
                  if(!empty($newtids)){
                    _personal_taxonomy_create_entity($entity->uid, $newtids);
                  }          
                }
              }            
            }
          }
        }
      }
    } 
  }
}


function _personal_taxonomy_create_entity($uid, $tids){
  foreach ($tids as $k => $tid){
    $values = array (
          'tid' => $tid,  //term id
          'uid' => $uid,
          'date' => time(),
    );
    $entity = entity_create('personal_taxonomy', $values);
    $save = entity_save('personal_taxonomy', $entity);
  }
}